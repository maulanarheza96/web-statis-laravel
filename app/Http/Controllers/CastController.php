<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class CastController extends Controller
{
    public function create()
    {
        return view('cast.create');
    }

    public function store(Request $request)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'nama harus diisi',
                'umur.required'  => 'umur harus diisi',
                'bio.required'  => 'bio harus diisi',
            ]
        );

        DB::table('cast')->insert(
            [
                'nama' => $request['nama'],
                'umur' => $request['umur'],
                'bio' => $request['bio']

            ]
        );

        return redirect('/cast');
    }

    public function index()
    {
        $Cast = DB::table('cast')->get();

        return view('cast.index', compact('Cast'));
    }


    public function show($id)
    {
        $Cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.show', compact('Cast'));
    }

    public function edit($id)
    {
        $Cast = DB::table('cast')->where('id', $id)->first();
        return view('cast.edit', compact('Cast'));
    }

    public function update(Request $request, $id)
    {
        $request->validate(
            [
                'nama' => 'required',
                'umur' => 'required',
                'bio' => 'required',
            ],
            [
                'nama.required' => 'nama harus diisi',
                'umur.required'  => 'umur harus diisi',
                'bio.required'  => 'bio harus diisi',
            ]
        );

        DB::table('Cast')
            ->where('id', $id)
            ->update(
                [
                    'nama' => $request['nama'],
                    'umur' => $request['umur'],
                    'bio' => $request['bio'],
                ]
            );

        return redirect('/cast');
    }

    public function destroy($id)
    {
        DB::table('cast')->where('id', '=', $id)->delete();

        return redirect('/cast');
    }
}
