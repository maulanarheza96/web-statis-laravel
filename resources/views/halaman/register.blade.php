@extends('layout.master')

@section('judul')
    Form Pendaftaran
@endsection

@section('isi')
    <h1> Buat Account Baru! </h1>
    <h3> Sign Up Form </h3>
    <form action="/welcome" method="POST">
        @csrf
        <label> First Name : </label> <br><br>
        <input type="text" name="firstname"><br><br>
        <label> Last Name : </label><br><br>
        <input type="text" name="lastname"><br><br>
        <label> Gender : </label><br><br>
        <input type="radio" name="Gender"> Male<br>
        <input type="radio" name="Gender"> Female<br>
        <input type="radio" name="Gender"> Other<br><br>
        <label> Nationality : </label><br><br>
        <select name="WN">
            <option value="Indonesian">Indonesian</option>
            <option value="Korean">Korean</option>
            <option value="Malaysian">Malaysian</option>
            <option value="etc">etc</option><br><br>
        </select><br><br>
        <label> Language Spoken : </label><br><br>
        <input type="checkbox" name="LS"> Bahasa Indonesia<br>
        <input type="checkbox" name="LS"> English<br>
        <input type="checkbox" name="LS"> Other<br><br>
        <label> Bio : </label><br><br>
        <textarea name="bio" cols=30 rows=10></textarea><br><br>
        <button type="submit" class="signup">Sign Up</button>
    </form>
@endsection