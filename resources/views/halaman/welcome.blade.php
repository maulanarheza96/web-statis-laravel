@extends('layout.master')

@section('judul')
    Welcome
@endsection

@section('isi')
<h1>Selamat Datang {{ $firstname }} {{ $lastname }}</h1>
<h2>Terima kasih telah bergabung di SanberBook. Social kita bersama!</h2>
@endsection