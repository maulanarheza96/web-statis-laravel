@extends('layout.master')

@section('judul')
    Detail Cast {{$Cast->nama}}
@endsection

@section('isi')

<h1> {{ $Cast->nama }} </h1>
<p> {{ $Cast->umur }} </p>
<p> {{ $Cast->bio }} </p>

<a href="/cast" class="btn btn-secondary">Kembali</a>
@endsection